set_property SRC_FILE_INFO {cfile:c:/Users/Gebruiker/Documents/pxl/FPGA/Wifi_Pmod/Wifi_Pmod.srcs/sources_1/bd/WIFI/ip/WIFI_processing_system7_0_0/WIFI_processing_system7_0_0.xdc rfile:../Wifi_Pmod.srcs/sources_1/bd/WIFI/ip/WIFI_processing_system7_0_0/WIFI_processing_system7_0_0.xdc id:1 order:EARLY scoped_inst:WIFI_i/processing_system7_0/inst} [current_design]
set_property SRC_FILE_INFO {cfile:{C:/Users/Gebruiker/Documents/pxl/EA-ICT 1ste jaar/VHDL/MiniZed_Constraints_Rev1_170613.xdc} rfile:{../../../EA-ICT 1ste jaar/VHDL/MiniZed_Constraints_Rev1_170613.xdc} id:2} [current_design]
set_property src_info {type:SCOPED_XDC file:1 line:21 export:INPUT save:INPUT read:READ} [current_design]
set_input_jitter clk_fpga_0 0.6
set_property src_info {type:SCOPED_XDC file:1 line:24 export:INPUT save:INPUT read:READ} [current_design]
set_input_jitter clk_fpga_1 0.3
set_property src_info {type:XDC file:2 line:62 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict{ PACKAGE_PIN R8 IOSTANDARD LVCMOS33} [get_ports {btns_4bits[0]    }];  # "R8.ARDUINO_IO0"
set_property src_info {type:XDC file:2 line:63 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict{ PACKAGE_PIN P8 IOSTANDARD LVCMOS33} [get_ports {btns_4bits[1]    }];  # "P8.ARDUINO_IO1"
set_property src_info {type:XDC file:2 line:64 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict{ PACKAGE_PIN P9 IOSTANDARD LVCMOS33} [get_ports {btns_4bits[2]    }];  # "P9.ARDUINO_IO2"
set_property src_info {type:XDC file:2 line:65 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict{ PACKAGE_PIN R7 IOSTANDARD LVCMOS33} [get_ports {btns_4bits[3]    }];  # "R7.ARDUINO_IO3"
set_property src_info {type:XDC file:2 line:66 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict{ PACKAGE_PIN N7 IOSTANDARD LVCMOS33} [get_ports {swts_4bits[0]    }];  # "N7.ARDUINO_IO4"
set_property src_info {type:XDC file:2 line:67 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict{ PACKAGE_PIN R10 IOSTANDARD LVCMOS33} [get_ports {swts_4bits[1]    }];  # "R10.ARDUINO_IO5"
set_property src_info {type:XDC file:2 line:68 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict{ PACKAGE_PIN P10 IOSTANDARD LVCMOS33} [get_ports {swts_4bits[2]    }];  # "P10.ARDUINO_IO6"
set_property src_info {type:XDC file:2 line:69 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict{ PACKAGE_PIN N8 IOSTANDARD LVCMOS33} [get_ports {swts_4bits[3]    }];  # "N8.ARDUINO_IO7"
set_property src_info {type:XDC file:2 line:70 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict{ PACKAGE_PIN M9 IOSTANDARD LVCMOS33} [get_ports {leds_4bits[0]    }];  # "M9.ARDUINO_IO8"
set_property src_info {type:XDC file:2 line:71 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict{ PACKAGE_PIN N9 IOSTANDARD LVCMOS33} [get_ports {leds_4bits[1]    }];  # "N9.ARDUINO_IO9"
set_property src_info {type:XDC file:2 line:72 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict{ PACKAGE_PIN M10 IOSTANDARD LVCMOS33} [get_ports {leds_4bits[2]   }];  # "M10.ARDUINO_IO10"
set_property src_info {type:XDC file:2 line:73 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict{ PACKAGE_PIN M11 IOSTANDARD LVCMOS33} [get_ports {leds_4bits[3]   }];  # "M11.ARDUINO_IO11"
